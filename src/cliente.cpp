#include "cliente.hpp"

Cliente::Cliente(){
}

//Construtor com sobrecarga
Cliente::Cliente(string CPF, string nome, string email, string telefone){
    this->CPF = CPF;
    this->nome = nome;
    this->email = email;
    this->telefone = telefone;
}

//CPF 
string Cliente::getCPF(){
    return this->CPF;
}

void Cliente::setCPF(string CPF){
    this->CPF = CPF;
}

//Nome
string Cliente::getNome(){
    return this->nome;
}

void Cliente::setnome(string nome){
    this->nome = nome;
}

//Telefone
string Cliente::getTelefone(){
    return this->telefone;
}

void Cliente::setTelefone(string telefone){
    this->telefone = telefone;
}

//email
string Cliente::getEmail(){
    return this->email;
}

void Cliente::setEmail(string email){
    this->email = email;
}

void Cliente::leCategorias(string CPF){
    string aux;
    ifstream arquivo("arq/Categorias.txt", ios::in);
    while(arquivo >> aux){
        learqCliente(CPF, aux);
    }
    geraLista(this->salvaCategorias);
    arquivo.close();
}

void Cliente::learqCliente(string CPF, string categoria){
    int cont = 0;
    unsigned int i;
    categorias temp;
    ifstream arquivo;
    string aux;
    string nome_arquivo = "arq/" + CPF + "compra";
    arquivo.open(nome_arquivo.c_str(), ios::in);
    while(arquivo >> aux){
        if(categoria == aux){
            for (i = 0; i< salvaCategorias.size(); i++){
                if(categoria == salvaCategorias[i].categoria){
                    salvaCategorias[i].quantidade++;
                    cont = 1;
                }
            }
            if (cont == 0){
                temp.categoria = aux;
                temp.quantidade++;
                salvaCategorias.push_back(temp);
            }
        }
    }
    arquivo.close();
}

bool Cliente::maior_menor_categorias(categorias A, categorias B){
    return A.quantidade > B.quantidade;
}

bool Cliente::maior_menor_relevancia(relevancia A, relevancia B){
    return A.quantidade > B.quantidade;
}

 void Cliente::menu(){
    string CPF;
    int verifica = 1, i = 0;
    cpf:
    cout << "\t=========================================" << endl;
    cout << "\t======== *LOJINHO : RECOMENDAÇÃO* =======" << endl;
    cout << "\t=========================================" << endl;
    cout << "\n\tCPF -> ";
    cin >> CPF;

    verifica = verifica_CPF(CPF);
    if(verifica == 1){
        cout << "CPF não cadastrado!" <<  endl;
        cout << "Por favor, digite um CPF já cadastrado" << endl;
        i++;
        sleep(1.0);
        goto cpf;
    }     
    else {
        leCategorias(CPF);
    }
} 

void Cliente::geraLista(vector<categorias> categorias_cliente){
    relevancia aux2;
    ifstream arquivo;
    string nome_arquivo, aux;
    unsigned int j, i;
    int cont = 0;
    
    sort(categorias_cliente.begin(), categorias_cliente.end(), maior_menor_categorias);    
    for(j=0; j<categorias_cliente.size(); j++){
        nome_arquivo = "arq/" + categorias_cliente[j].categoria;
        arquivo.open(nome_arquivo.c_str(), ios::in);
        while (arquivo >> aux){
            for (i = 0; i<lista_recomendacao.size(); i++){
                if(aux == lista_recomendacao[i].produto.getCodigo()){
                    lista_recomendacao[i].quantidade++;
                    cont = 1;
                }
            }
            if (cont == 0){
                aux2.produto = lerArquivoProduto(aux);
                aux2.quantidade = 1;
                lista_recomendacao.push_back(aux2);  
            }
        }
        arquivo.close();
    }
    sort(lista_recomendacao.begin(), lista_recomendacao.end(), maior_menor_relevancia);
    imprimeLista();
}

 void Cliente::imprimeLista(){
    unsigned int i;
    cout << "Lista de produtos recomendados para o cliente: " << endl;
    if (lista_recomendacao.size() > 10){
        for (i = 0; i <10; i++){
            cout << lista_recomendacao[i].produto.getNome() << endl;
            sleep(0.3);
        }
    } else {
        for (i = 0; i <lista_recomendacao.size(); i++){
            cout << lista_recomendacao[i].produto.getNome() << endl;
            sleep(0.3);            
        }
    } 
    sleep(2);
}   

Produto Cliente::lerArquivoProduto(string codigo){
    int i=1;
    Produto aux;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(arquivo >> leitor){
                switch (i){
                case 1:
                    aux.setCodigo(leitor);
                    break;
                case 2:
                    aux.setNome(leitor);
                    break;
                case 3:
                    aux.setPreco(stof(leitor));
                    break;
                case 4:
                    aux.setQuantidade(stoi(leitor));
                    break;
                default:
                    aux.setCategorias(leitor);
                    break;
                }
                i++;
            }
        }
    arquivo.close();
    return(aux);
}

int Cliente::verifica_CPF(string CPF){
    int resultado = 1;
    string aux;
    ifstream arquivo("arq/CPFS.txt", ios::in);
    
    arquivo.seekg(0);
    while (arquivo >> aux){
        if(aux == CPF){
            resultado = 0;
        }
    }
    return resultado;
}
