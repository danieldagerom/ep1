#include "lojinho_estoque.hpp"
Estoque::Estoque(){
}

void Estoque::menu(){
    int escolha_menu, atualiza_quantidade;
    string codigo;
    system("clear");    
    cout << "\t=========================================" << endl;
    cout << "\t========== *LOJINHO : ESTOQUE* ==========" << endl;
    cout << "\t=========================================" << endl;
    cout << "\t========== (1) NOVO PRODUTO =============" << endl;
    cout << "\t========== (2) ATUALIZAR PRODUTO ========" << endl;
    cout << "\t=========================================" << endl;
    cout << "\nQual opção deseja? -> ";
    cin >> escolha_menu;
    switch (escolha_menu){
        case 1:
            leProduto();
            break;
        case 2:
            cout << "Código -> ";
            __fpurge(stdin);
            getline(cin, codigo); 
            if(verifica_codigo(codigo) == 0){
                cout << "Código de produto não consta no estoque!\n";
                sleep(2.0);
            }else {
                cout << "Quantidade para atualizar -> ";
                cin >> atualiza_quantidade;
                mudaQuantidade(codigo, atualiza_quantidade);
                cout << "Produto " << codigo << " atualizado com sucesso!" << endl;
                sleep(1);
            }
        break;
    }
}

void Estoque::leProduto(){
    Produto *leproduto;
    string nome, marca, cor, codigo, tema, categoria;
    char escolha_categoria;
    float preco;
    int quantidade;
    leproduto = new Produto();
    do{
        cout << "Codigo -> ";
        
            __fpurge(stdin);
            getline(cin, codigo);
            if (verifica_codigo(codigo) == 0)
                leproduto -> setCodigo(codigo);

            else{
               cout << "Produto já cadastrado!\n";
               sleep(2);
            }
    }while (verifica_codigo(codigo)!= 0);

    cout << "Nome -> ";
    getline(cin, nome);
    leproduto->setNome(nome);

    do {
        cout << "Categoria -> ";
        cin >> categoria;
        leproduto->setCategorias(categoria);
        cout << "Deseja incluir mais uma categoria? (s/n) -> ";
        cin >> escolha_categoria;
    } while (escolha_categoria == 's' || escolha_categoria == 'S');

    cout << "Preço -> ";
    cin >> preco;
    leproduto->setPreco(preco);

    cout << "Quantidade -> " << endl;
    cin >> quantidade;
    leproduto -> setQuantidade(quantidade);
    grava_produto(leproduto);
}

void Estoque::grava_produto(Produto *grava_produto){
    unsigned int i;
    ofstream arquivo;
    string nome_arquivo = "arq/" + grava_produto->getCodigo();
    arquivo.open ( nome_arquivo.c_str(), ios::app | ios::out);
        if(arquivo.is_open()){
            arquivo << grava_produto->getCodigo()<< endl;
            arquivo << grava_produto->getNome()<< endl;
            arquivo << grava_produto->getPreco()<< endl;
            arquivo << grava_produto->getQuantidade()<< endl;

            for (i = 0; i < grava_produto->getCategorias().size(); i++){
                arquivo << grava_produto->getCategorias()[i] << endl; 
                grava_categoria(grava_produto, i);
                verifica_categoria(grava_produto->getCategorias()[i]);
            }
        }
    arquivo.close();
    grava_codigo(grava_produto);
    cout << "Produto cadastrado com sucesso!" << endl;
}

void Estoque::grava_codigo(Produto *grava_codigo){
    ofstream arquivo("arq/Codigos.txt", ios::app | ios::out);
        if(arquivo.is_open()){
            arquivo << grava_codigo->getCodigo()<< endl;
        }
    arquivo.close();
}

void Estoque::grava_categoria(Produto *grava_categoria, int i){    
    ofstream arquivo;
    string nome_arquivo = "arq/" + grava_categoria->getCategorias()[i];
    arquivo.open ( nome_arquivo.c_str(), ios::app | ios::out);
    if(arquivo.is_open()){
            arquivo << grava_categoria->getCodigo() << endl;
        }
    arquivo.close();
}

void Estoque::grava_categoria(string categoria){
    ofstream arquivo("arq/Categorias.txt", ios::out| ios::app);
    if (arquivo.is_open()){
        arquivo << categoria << endl;
    }
    arquivo.close();
}

void Estoque::verifica_categoria(string categoria){
    int i = 0;
    string aux;
    ifstream arquivo("arq/Categorias.txt", ios::in);
    arquivo.seekg(0);
    if (arquivo.is_open()){
        while(arquivo >> aux){
            if (aux == categoria){
                i = 1;
                break;
            }
        }
    }
    arquivo.close();
    if (i == 0){
        grava_categoria(categoria);
    }
}

int Estoque::verifica_codigo(string codigo){
    int resultado = 0;
    string aux;
    ifstream arquivo("arq/Codigos.txt", ios::in);
    
    arquivo.seekg(0);
    while (arquivo >> aux){
        if(aux == codigo){
            resultado = 1;
        }
    }
    return resultado;
}

int Estoque::le_quantidade(string codigo_le_quantidade){
    string le_quantidade, quantidade;
    int i = 1, quantidade_inteiro;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo_le_quantidade;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while (arquivo >> le_quantidade){
                if (i == 7){
                    quantidade = le_quantidade;
                }
                i++;
            }
        }
    arquivo.close();
    quantidade_inteiro = stoi(quantidade);
    return quantidade_inteiro;
}

void Estoque::mudaQuantidade(Produto *mudaquantidade, int quantidade){
    unsigned int i;
    ofstream arquivo;
    //cout << "inicio muda quantidade ==== " <<  endl;
    string nome_arquivo = "arq/" + mudaquantidade->getCodigo();
    arquivo.open(nome_arquivo.c_str(), ios::trunc | ios::out);
        if(arquivo.is_open()){
            arquivo << mudaquantidade->getCodigo()<< endl;
            arquivo << mudaquantidade->getNome()<< endl;
            arquivo << mudaquantidade->getPreco()<< endl;
            int quant = (mudaquantidade->getQuantidade()+quantidade);
            arquivo << quant << endl;
            for (i = 0; i < mudaquantidade->getCategorias().size(); i++){
               arquivo << mudaquantidade->getCategorias()[i]<< endl; 
            }
        }
    arquivo.close();
}

void Estoque::mudaQuantidade(string codigo_mudaquantidade, int quantidade){
    int i = 1;
    Produto *aux;
    string le_quantidade;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo_mudaquantidade;
    aux = new Produto();
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(arquivo >> le_quantidade){
                switch (i)
                {
                case 1:
                    aux->setCodigo(le_quantidade);
                    break;
                case 2:
                    aux->setNome(le_quantidade);
                    break;
                case 3:
                    aux->setPreco(stof(le_quantidade));
                    break;
                case 4:
                    aux->setQuantidade(stoi(le_quantidade));
                    break;
                default:
                    aux->setCategorias(le_quantidade);
                    break;
                }
                i++;
            }
        }
    arquivo.close();
    mudaQuantidade(aux, quantidade);
}
