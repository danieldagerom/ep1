#include <cadastro.hpp>

Cadastro::Cadastro(){
}

void Cadastro::le_cliente(){
    Cliente *le_cliente;
    string nome, CPF, email, telefone;
    char escolha_socio;
    Socio cadastro_socio;

    le_cliente = new Cliente();

    __fpurge(stdin);
    cout << "Nome Completo -> ";
    getline(cin, nome);
    le_cliente->setnome(nome);
    do{
        cout << "CPF -> ";
        getline(cin, CPF);
        if (verifica_CPF(CPF) == 0)
            le_cliente->setCPF(CPF);
        else{
            cout << "CPF já consta no sistema!" << endl;
            cout << "Digite um CPF não cadastrado ainda!" << endl;
            sleep(1);
        }
    } while (verifica_CPF(CPF) == 1);
    
    cout << "Email -> ";
    getline(cin, email);
    le_cliente->setEmail(email);
    cout << "Telefone -> ";
    getline(cin, telefone);
    le_cliente->setTelefone(telefone);
    grava_cliente(le_cliente);
    
    cout << "Insira 's' para cadastrar como socio (OPCIONAL): ";
    cin >> escolha_socio;
        if (escolha_socio == 's' || escolha_socio == 'S'){
            cadastro_socio.cadastraSocio(*le_cliente);
            cout << "Cliente, " << nome << "já consta no sitema como sócio!" << endl;
            sleep(2);
        } else {
            cout << endl;
        }
    cout << "Cadastro concluído com sucesso!" << endl;
    sleep(2);
}

int Cadastro::verifica_CPF(string CPF){
    int resultado = 0;
    string aux;
    ifstream arquivo("arq/CPFS.txt", ios::in);
    
    arquivo.seekg(0);
    while (arquivo >> aux){
        if(aux == CPF){
            resultado = 1;
        }
    }
    return resultado;
}

void Cadastro::grava_cliente(Cliente *grava_cliente){
    ofstream arquivo;
    string nome_arquivo ="arq/"+grava_cliente->getCPF();
    arquivo.open (nome_arquivo.c_str(), ios::app | ios::out);
        if(arquivo.is_open()){
            arquivo << grava_cliente->getCPF()<< endl;
            arquivo << grava_cliente->getEmail()<< endl;
            arquivo << grava_cliente->getTelefone()<< endl;
            arquivo << grava_cliente->getNome()<< endl;
        }
    arquivo.close();
    this->cadastro_cliente = *grava_cliente;
    grava_CPF(grava_cliente);
    cout << "Dados gravados com sucesso!" << endl;
}

void Cadastro::grava_CPF(Cliente *grava_cpf){
    ofstream arquivo("arq/CPFS.txt", ios::app | ios::out);
        if(arquivo.is_open()){
            arquivo << grava_cpf->getCPF()<< endl;
        }
    arquivo.close();
}

Cliente Cadastro::getCadastro_cliente(){
    return this->cadastro_cliente;
}