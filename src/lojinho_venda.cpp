#include "lojinho_venda.hpp"
Venda::Venda(){
}

//Insere o cliente da venda;
void Venda::solicitaCliente(){
    int escolha_cliente;
    char escolha_socio;
    string CPF;
    Cadastro cadastro_cliente;
    Socio cadastro_socio;

    system("clear");    
    cout << "\t=========================================" << endl;
    cout << "\t============ *LOJINHO : VENDA* ==========" << endl;
    cout << "\t=========================================" << endl;
    cout << "\t========== (1) NOVO CADASTRO ============" << endl;
    cout << "\t========== (2) CLIENTE JÁ CADASTRADO ====" << endl;
    cout << "\t=========================================" << endl;
    cout << "\nQual opção deseja? -> ";
    cin >> escolha_cliente;
    switch (escolha_cliente){
    case 1:
        cadastro_cliente.le_cliente();
        this->cliente = cadastro_cliente.getCadastro_cliente();
        inserirProdutos();
        break;
    case 2:
        cout << "CPF -> ";
        cin >> CPF;
        if (buscaCliente(CPF) == 0){
            this->cliente = lerArquivoCliente(CPF);
            cout << "Cliente: " << cliente.getNome() << endl;
            if (verificaSocio() == 1){
                cout << "Cliente, " << cliente.getNome() << " não consta como sócio!" << endl;
                cout << "Digite 's' para cadastrá-l@ como sócio (OPCIONAL): ";
                cin >> escolha_socio;
                if (escolha_socio == 's' || escolha_socio == 'S'){
                    cadastro_socio.cadastraSocio(this->cliente);
                    cout << "Cliente, " << cliente.getNome() << " cadastrado como sócio!" << endl;
                    sleep(1);
                } else {
                    cout << endl;
                }
            } else {
                cout << "Cliente, " << cliente.getNome() << " já consta como sócio!" << endl;
                sleep(1);
            }
        }else {
            cout << "Cliente, " << cliente.getNome() << " ainda não cadastrado!" << endl;
            sleep(1);
            cout << "NOVO CADASTRO -> " << endl;
            cadastro_cliente.le_cliente();
            this->cliente = cadastro_cliente.getCadastro_cliente();
        }
        inserirProdutos();
        break;
    default:
        cout << "Opção inválida!" << endl;
        sleep(1);
        solicitaCliente();
        break;
    }
}

//Verifica se o cliente está cadastrado;
int Venda::buscaCliente(string CPF){
    int resultado = 1;
    string aux;
    ifstream arquivo("arq/CPFS.txt", ios::in);
    
    arquivo.seekg(0);
    while (arquivo >> aux){
        if(aux == CPF){
            resultado = 0;
        }
    }
    return resultado;
}

//Insere os produtos da venda;
void Venda::inserirProdutos(){
    Estoque controle_quantidade;
    int indice = 0;
    char escolha = 's';
    string codigo;
    int quantidade;
    do{
        cout << "Código -> ";
        cin >> codigo;
        this->produtos.push_back(lerArquivoProduto(codigo));
        cout << produtos[indice].getNome() << endl;
        cout << "Quantidade -> ";
        cin >> quantidade;
        this->quantidades.push_back(quantidade);
        cout << "Adicionar mais um produto? (s/n)" << endl;
        cin >> escolha;
        indice ++;
    } while (escolha == 's' || escolha == 'S');
    verificaProdutos(produtos, quantidades);
}

void Venda::verificaProdutos(vector<Produto> produtos, vector<int> quantidades){
    unsigned int i;
    int aux = 0;
    for (i = 0; i < produtos.size(); i++){
        if (verificaEstoque(produtos[i].getCodigo(), quantidades[i]) == -1){
            aux = 1;
            cout << "Quantidade insuficiente no estoque!\n";
            sleep(2);
        }
    }

    if (aux == 0){
        cout << "aux = 0" << endl;
        mudaEstoque(produtos, quantidades);
    }
}

void  Venda::mudaEstoque(vector<Produto> produtos, vector<int>quantidades){
    unsigned int i;
    Estoque controle_quantidade;
    cout << "inicio muda estoque" << endl;
    
    for (i = 0; i < produtos.size(); i++){
        controle_quantidade.mudaQuantidade(produtos[i].getCodigo(), -quantidades[i]);
    }
    calculaValor();
    cout << "funcao valor ======" << endl;
    calculaDesconto();
    cout << "funcao desconto ======" << endl;
    resumoVenda();
    sleep(3.0);
    cout << "funcao venda ======" << endl;
    salvaCompra();
    cout << "funcao salvacompra =====" << endl;
}

//Calcula valor parcial;
void Venda::calculaValor(){
    unsigned int i;
    float valor = 0;

    for (i =0; i < this->produtos.size(); i ++){
        valor = valor + (produtos[i].getPreco()*quantidades[i]);
    }
    this->valor_parcial = valor;
}

//Lê as informações do arquivo do produto e retorna um Produto;
Produto Venda::lerArquivoProduto(string codigo){
    int i = 1;
    Produto aux;
    float preco;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(getline(arquivo, leitor)){
                if(leitor != "") {
                    switch (i){
                    case 1:
                        aux.setCodigo(leitor);
                        break;
                    case 2:
                        aux.setNome(leitor);
                        break;
                    case 3:
                        preco = stof(leitor);
                        aux.setPreco(preco);
                        break;
                    case 4:
                        aux.setQuantidade(stoi(leitor));
                        break;
                    default:
                        aux.setCategorias(leitor);
                        break;
                    }
                    i++;
                }
            }
        }
    arquivo.close();
    return(aux);
}

//Verifica a quantidade de produtos do Produto;
int Venda::verificaEstoque(string codigo, int quantidade){
    Produto produto_verifica = lerArquivoProduto(codigo);
    if (produto_verifica.getQuantidade() < quantidade){
        return -1;
    } else {
        return 0;
    }
}

//Lê o arquivo do cliente e retorna um Cliente;
Cliente Venda::lerArquivoCliente(string CPF){
    int i = 1;
    Cliente aux;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + CPF;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(arquivo >> leitor){
                switch (i){
                case 1:
                    aux.setCPF(leitor);
                    break;
                case 2:
                    aux.setEmail(leitor);
                    break;
                case 3:
                    aux.setTelefone(leitor);
                    break;
                case 4:
                    aux.setnome(leitor);
                    break;
                default:
                    break;
                }
                i++;
            }
        }
    arquivo.close();
    return(aux);
}

//Verifica se o cliente é sócio;
int Venda::verificaSocio(){
    int resultado = 1;
    string aux;
    ifstream arquivo("arq/Socios.txt", ios::in);
    
    arquivo.seekg(0);
    while (arquivo >> aux){
        if(aux == cliente.getCPF()){
            resultado = 0;
        }
    }
    return resultado;
}

//Calcula o desconto de sócio;
void Venda::calculaDesconto(){
    float desconto;
    desconto = this->valor_parcial * 0.15;
    this->desconto = desconto;
}
 
//Apresenta o resumo da venda;
void Venda::resumoVenda(){
    unsigned int i;
    
    cout << endl << "Resumo da venda: " << endl;
    cout << this->cliente.getNome() << endl;
    for (i=0; i < this->produtos.size(); i ++){
        cout << this->produtos[i].getNome() << "(R$" << this->produtos[i].getPreco() << ") x " << this->quantidades[i] << endl;
    }
    cout << "Valor total: R$" << this->valor_parcial << endl;
    if (verificaSocio() == 0){
        cout.precision(2);
        cout << "Desconto de sócio(15%): " <<"R$"<< this->desconto << endl;
        this->valor_total = this->valor_parcial - this->desconto;
    } else {
        cout << "Sem desconto de sócio;" << endl;
        this->valor_total = this->valor_parcial;
    }
    cout.precision(2);
    cout << "Valor final: R$" << this->valor_total << endl;
}


void Venda::setProdutos(Produto produto){
    this->produtos.push_back(produto);
}


void Venda::salvaCompra(){
    unsigned int i, j;
    ofstream arquivo;
    string nome_arquivo = "arq/" + this->cliente.getCPF() + "compra";
    arquivo.open(nome_arquivo.c_str(), ios::out|ios::app);
    for (i=0; i <produtos.size(); i++){
        for (j = 0; j<produtos[i].getCategorias().size(); j++){
            if (arquivo.is_open()){
                arquivo << produtos[i].getCategorias()[j]<<endl;
            }
        }
    }
}

