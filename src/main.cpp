#include <iostream>
#include <fstream>
#include <string>
#include "cliente.hpp"
//#include "funcoes.hpp"
#include "lojinho_venda.hpp"
#include "lojinho_estoque.hpp"
#include <vector>
#include <unistd.h>
//vector <string> vetor;

using namespace std;

int main(){
    int opc, cont = 0;
    Venda venda;
    Estoque estoque;
    Cliente recomenda;
    
    do{
    system("clear");    
    cout << "\t=========================================" << endl;
    cout << "\t========== *LOJINHO DA VICTORIA* ========" << endl;
    cout << "\t============= *BEM VINDO(A)!* ===========" << endl;
    cout << "\t=========================================" << endl;
    cout << "\t========== (1) Modo Venda ===============" << endl;
    cout << "\t========== (2) Modo estoque =============" << endl;
    cout << "\t========== (3) Modo Recomendacao ========" << endl;
    cout << "\t========== (0) Sair =====================" << endl;
    cout << "\n\tQual opção deseja? -> ";
    cin >> opc;

    switch (opc){
        case 1:
            cout << endl;
            cout << "Iniciando *LOJINHO VENDA*..." << endl;
            sleep(1);
            venda.solicitaCliente();
            break;
        case 2:
            cout << "Iniciando *LOJINHO ESTOQUE*..." << endl;
            sleep(1);
            estoque.menu();
            break;
        case 3:
            cout << "Iniciando *LOJINHO RECOMENDAÇÃO*..." << endl;
            sleep(1);
            recomenda.menu();
            break;
        case 0:
            cout << "Encerrando..."<<endl;
            sleep(1.5);
            break;
        default:
            cout << "\nOpcao invalida!"<<endl;
            sleep(1.5);
            break;
        }
    }while(opc != 0);

    return 0;
}