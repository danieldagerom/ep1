# INSTRUÇÕES DE USO:
*Abra seu terminal (Ctrl + Alt + t)

*Cole o seguinte comando:

$  git clone https://gitlab.com/danieldagerom/ep1

*Após colar no diretório de sua preferencia, utilize o seguinte comando para compilar:

$  make

*Depois de compilar, utilize o seguinte comando para executar:

$  make run

*Caso queria finalizar o programa a qualquer momento, utilize o atalho (Ctrl + c)

OBS (O programa apresenta alguns erros, por ter sido feito já nos últimos dias)