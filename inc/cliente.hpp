#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <unistd.h>
#include "produto.hpp"


using namespace std;

struct categorias{
        string categoria;
        int quantidade = 0;
};
typedef struct categorias categorias;

struct relevancia{
    Produto produto;
    int quantidade = 0;
};
typedef struct relevancia relevancia;

class Cliente{
    private:
        string CPF;
        string nome;
        string email;
        string telefone;
        char socio;
        vector<categorias> salvaCategorias;
        vector<relevancia> lista_recomendacao;
        void geraLista(vector<categorias> categorias_cliente);
        static bool maior_menor_categorias(categorias A, categorias B);
        static bool maior_menor_relevancia(relevancia A, relevancia B);
        void imprimeLista();
        void learqCliente(string CPF, string categoria);
        Produto lerArquivoProduto(string codigo);
        int verifica_CPF(string CPF);
    public:
        Cliente();
        Cliente(string CPF, string nome, string email, string telefone);
        //~Cliente();
        string getCPF();
        void setCPF(string CPF);
        string getNome();
        void setnome(string nome);
        string getEmail();
        void setEmail(string email);
        string getTelefone();
        void setTelefone(string telefone);
        void leCategorias(string CPF);
        void menu();
};

#endif