#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Produto{
    private:
    string nome;
        float preco;
        string marca;
        string codigo;
        int quantidade;
        vector<string> categorias;
    public:
        Produto();
        ~Produto();
        string getNome();
        void setNome(string nome);
        float getPreco();
        void setPreco(float preco);
        string getMarca();
        void setMarca(string marca);
        string getCodigo();
        void setCodigo(string codigo);
        int getQuantidade();
        void setQuantidade(int quantidade);
        vector<string> getCategorias();
        void setCategorias(string categoria);
};

#endif