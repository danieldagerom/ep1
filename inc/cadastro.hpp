#ifndef CADASTRO_HPP
#define CADASTRO_HPP
#include <iostream>
#include <string>
#include <string.h>
#include <fstream>
#include <stdio_ext.h>
#include <unistd.h>
#include "cliente.hpp"
#include "socio.hpp"
//#include "lojinho_venda.hpp"

using namespace std;

class Cadastro{
    private:
        Cliente cadastro_cliente;
        void grava_cliente(Cliente *grava_cliente);
        void grava_CPF(Cliente *grava_cpf);
        int verifica_CPF(string CPF);
    public:
        Cadastro();
        void le_cliente();
        Cliente getCadastro_cliente();
};

#endif