#ifndef SOCIO_HPP
#define SOCIO_HPP
#include <iostream>
#include <string>
#include "cliente.hpp"

class Socio : public Cliente{
        
    public:
        Socio();
        void cadastraSocio(Cliente clienteSocio);
};


#endif