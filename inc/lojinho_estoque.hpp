#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP
#include <string>
#include <string.h>
#include <fstream>
#include <vector>
#include <unistd.h>
#include <stdio_ext.h>
#include "produto.hpp"
using namespace std;

class Estoque{
    private:
        Produto *cadastra_produto;
        void grava_produto(Produto *grava_produto);
        void grava_codigo(Produto *grava_codigo);
        int verifica_codigo(string verifica_codigo);
        void grava_categoria(Produto *grava_categoria, int i);
        void verifica_categoria(string categoria);
        void grava_categoria(string categoria);
    public:
        Estoque();
        void leProduto();
        void mudaQuantidade(Produto *mudaquantidade, int quantidade);
        void mudaQuantidade(string codigo_mudaquantidade, int quantidade);
        int le_quantidade(string codigo_le_quantidade);
        void menu();
};

#endif