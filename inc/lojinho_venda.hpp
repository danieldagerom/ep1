#ifndef LOJINHO_VENDA_HPP
#define LOJINHO_VENDA_HP
#include <iostream>
#include <string>
#include <vector>
#include <stdio_ext.h>
#include "socio.hpp"
#include "cliente.hpp"
#include "produto.hpp"
#include "cadastro.hpp"
#include "lojinho_estoque.hpp"

using namespace std;

class Venda{
    private:

        vector<Produto> produtos;
        vector<int> quantidades;
        float valor_parcial;
        float valor_total;
        Cliente cliente;
        float desconto;
        Produto lerArquivoProduto(string codigo);
        Cliente lerArquivoCliente(string CPF);
        void calculaDesconto();
        void calculaValor();
        int buscaCliente(string CPF);
        void resumoVenda();
        int verificaSocio();
        void verificaProdutos(vector<Produto> produtos, vector<int> quantidades);
        void mudaEstoque(vector<Produto> produtos, vector<int>quantidades);
        int verificaEstoque(string codigo, int quantidade);
        void salvaCompra();

    public:
        Venda();
        void inserirProdutos();
        void setProdutos(Produto produto);
        float getValor();
        void setValor(float valor);
        Cliente getCliente();
        void setCliente(Cliente cliente);
        float getDesconto();
        void setDesconto(float desconto);
        void solicitaCliente();
};


#endif